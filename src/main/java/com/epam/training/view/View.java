package com.epam.training.view;

import com.epam.training.controller.StringUtils;
import com.epam.training.controller.TextController;
import com.epam.training.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class View {
    static final Logger logger = LogManager.getLogger(View.class.getName());
    private List<String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private TextController textController;

    public View() {
        scanner = new Scanner(System.in);
        menu = new LinkedList<>();
        methodsMenu = new LinkedHashMap<>();
        textController = new StringUtils();
    }

    public void run(){
        String keyMenu;
        do {
            generateMainMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        clearMenu();
        menu.add("1 - Change locale");
        menu.add("2 - Change text");
        menu.add("3 - Print..");
        menu.add("Q - Exit");
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        outputMenu();
    }



    private void pressButton1(){
        String keyMenu;
        do {
            generateLocaleMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                while (keyMenu.equals("")) {
                    keyMenu = scanner.nextLine().toUpperCase();
                }
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

    private void pressButton2() {
        String keyMenu;
        do {
            generateChangeTextMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                while (keyMenu.equals("")) {
                    keyMenu = scanner.nextLine().toUpperCase();
                }
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

    private void pressButton3() {
        String keyMenu;
        do {
            generatePrintTextMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                while (keyMenu.equals("")) {
                    keyMenu = scanner.nextLine().toUpperCase();
                }
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

    private void generateLocaleMenu() {
        clearMenu();
        menu.add("1 - Change to ukrainian");
        menu.add("2 - Change to english");
        menu.add("3 - Change to german");
        menu.add("4 - Change to french");
        menu.add("B - Back");
        methodsMenu.put("1", this::setUkrainianLocale);
        methodsMenu.put("2", this::setEnglishLocale);
        methodsMenu.put("3", this::setGermanLocale);
        methodsMenu.put("4", this::setFrenchLocale);
        outputMenu();
    }

    private void setUkrainianLocale(){}

    private void setEnglishLocale(){}

    private void setGermanLocale(){}

    private void setFrenchLocale(){}

    private void generateChangeTextMenu(){
        clearMenu();
        menu.add("1 - Task 5 - Swap first word starts by vowel letter by " +
                "longest word");
        menu.add("2 - Task 11 - In each sentence delete longest substring " +
                "which starts and ends by symbol");
        menu.add("3 - Task 12 - Delete words from text by given length ");
        menu.add("B - Back");
        methodsMenu.put("1", this::swapWords);
        methodsMenu.put("2", this::deleteLongestSubstring);
        methodsMenu.put("3", this::deleteAllWordsByLength);
        outputMenu();
    }

    private void swapWords(){
        textController.swapFirstWordStartsByVowelWithLongest();
    }

    private void deleteLongestSubstring(){
        logger.info("Enter symbol");
        Character c = scanner.next("\\w").charAt(0);
        textController.deleteLongestSubstring(c);
    }

    private void deleteAllWordsByLength(){
        logger.info("Enter length");
        int length = getInput();
        textController.deleteConsonantsByLength(length);
    }

    private int getInput(){
        int length;
        try{
            length = scanner.nextInt();
            while(length<1){
                logger.info("Wrong input, enter again");
                length = scanner.nextInt();
            }
        }catch (Exception e){
            scanner.reset();
            scanner.nextLine();
            return getInput();
        }
        return length;
    }

    private void generatePrintTextMenu(){
        clearMenu();
        menu.add("1 - Task 1 - Count max number of sentences with the same " +
                "word");
        menu.add("2 - Task 2 - Print sentences in word increasing order");
        menu.add("3 - Task 3 - Print word from 1st sentence word which exist " +
                "in every sentence");
        menu.add("4 - Task 4 - In all questions find and print words by " +
                "length");
        menu.add("5 - Task 6 - Print all words in alphabet order by first " +
                "letter");
        menu.add("6 - Task 7 - Print all words by vowel letter " +
                "percentage increasing order");
        menu.add("7 - Task 8 - Words which starts by vowel letter sort by " +
                "first consonant letter");
        menu.add("8 - Task 9 - Sort and print all words by increasing number " +
                "of given symbol");
        menu.add("9 - Task 10 - Show stats for given words & sort by " +
                "decreasing total occurrences order");
        menu.add("10 - Task 13 - Inverted task 9 (Decreasing order");
        menu.add("11 - Task 14 - Find longest palindrome from each sentence");
        menu.add("B - Back");
        methodsMenu.put("1", this::printMenuButton1);
        methodsMenu.put("2", this::printMenuButton2);
        methodsMenu.put("3", this::printMenuButton3);
        methodsMenu.put("4", this::printMenuButton4);
        methodsMenu.put("5", this::printMenuButton5);
        methodsMenu.put("6", this::printMenuButton6);
        methodsMenu.put("7", this::printMenuButton7);
        methodsMenu.put("8", this::printMenuButton8);
        methodsMenu.put("9", this::printMenuButton9);
        methodsMenu.put("10", this::printMenuButton10);
        methodsMenu.put("11", this::printMenuButton11);
        outputMenu();
    }

    private void printMenuButton1(){
        textController.findMaxNumberOfSentencesWithSameWord();
    }

    private void printMenuButton2(){
        textController.printSentencesInIncreasingOrder();
    }

    private void printMenuButton3(){
        textController.findUniqueWordInFirstSentence();
    }

    private void printMenuButton4(){
        logger.info("Enter length");
        int length = getInput();
        textController.printAllWordsFromQuestionsBy(length);
    }

    private void printMenuButton5(){
        textController.printWordsInAlphabetOrder();
    }

    private void printMenuButton6(){
        textController.sortByPercentageOfVowelLetters();
    }

    private void printMenuButton7(){
        textController.sortVowelWordsBy1stConsonantLetter();
    }

    private void printMenuButton8(){
        logger.info("Enter symbol");
        Character c = scanner.next("\\w").charAt(0);
        textController.sortByLetterOccurrenceIncreasing(c);
    }

    private void printMenuButton9(){
        textController.printWordOccurrences(fillSet());
    }

    private void printMenuButton10(){
        logger.info("Enter symbol");
        Character c = scanner.next("\\w").charAt(0);
        textController.sortByLetterOccurrenceDecreasing(c);
    }

    private void printMenuButton11(){
        textController.findLongestPalindromeInEachSentence();
    }

    public Set<Word> fillSet(){
        Set<Word> userWords = new HashSet<>();
        userWords.add(new Word("if"));
        userWords.add(new Word("the"));
        userWords.add(new Word("to"));
        return userWords;
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String option : menu) {
            System.out.println(option);
        }
        System.out.print(">>> ");
    }

    private void clearMenu(){
        menu.clear();
        methodsMenu.clear();
    }
}
