package com.epam.training.controller;

import com.epam.training.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringUtils implements TextController {
    static final Logger logger = LogManager.getLogger(TaskLogic.class.getName());
    TextModel textModel;
    Text text;

    public StringUtils() {
        textModel = new TaskLogic();
        text = textModel.getText();
    }

    @Override
    public void findMaxNumberOfSentencesWithSameWord() {
        Integer max = Integer.MIN_VALUE;
        Word mostCommonWord = null;
        Set<Word> words = text.getWordSet();
        for (Word w : words) {
            int count = (int) textModel.countSentencesWithWord(w);
            if (max < count) {
                max = count;
                mostCommonWord = w;
            }
        }
        logger.info("Most common word - \"" + mostCommonWord + "\", occurs in"
                + max + " sentences");
    }

    @Override
    public void printSentencesInIncreasingOrder() {
        List<Sentence> sentences = text.getSentences();
        Collections.sort(sentences,
                Comparator.comparingInt(Sentence::getSentenceLength)
                        .thenComparing(Sentence::toString));
        sentences.forEach(logger::info);
    }

    @Override
    public void findUniqueWordInFirstSentence() {
        Sentence fSentence = text.getSentences().get(0);
        Set<Word> words = fSentence.getWordsStat().keySet();
        for (Word word : words) {
            if (textModel.checkIfSentencesContain(word)) {
                logger.info(word + " occurs in every sentence");
                return;
            }
        }
        logger.warn("Such word doesn't exist in first sentence");
    }

    @Override
    public void printAllWordsFromQuestionsBy(int length) {
        List<Sentence> questions = text.getSentences().stream()
                .filter(sentence -> sentence.toString().endsWith("?"))
                .collect(Collectors.toList());
        Set<Word> words = new HashSet<>();
        questions.stream().forEach(sentence ->
                textModel.getWordsByLength(sentence, length)
                        .forEach(word -> words.add(word))
        );
        words.forEach(logger::info);
    }

    @Override
    public void swapFirstWordStartsByVowelWithLongest() {
        List<Sentence> sentences = text.getSentences();
        sentences.forEach(sentence -> {
            Sentence oldSentence = new Sentence(sentence.getSentence());
            sentence.setSentence(textModel.swapWords(sentence).getSentence());
            logger.debug(oldSentence.getSentence());
            logger.debug(sentence.getSentence());
            if (!sentence.getSentence().equals(oldSentence.getSentence())) {
                logger.info("Words swapped in sentence #"
                        + sentences.indexOf(sentence));
            } else {
                logger.error("Couldn't swap words in sentence #"
                        + sentences.indexOf(sentence));
            }
        });
    }

    @Override
    public void printWordsInAlphabetOrder() {
        List<Word> words = new ArrayList<>(textModel.getAllWorlds());
        List<List<Character>> lettersList = new LinkedList<>();
        words.forEach(word -> lettersList.add(word.getLetterList()));
        Collections.sort(lettersList, (o1, o2) -> {
            Iterator<Character> first = o1.iterator();
            Iterator<Character> second = o2.iterator();
            int ret = Integer.MIN_VALUE;
            if (first.hasNext() && second.hasNext()) {
                ret = first.next().toString()
                        .compareToIgnoreCase(second.next().toString());
            }
            while (ret == 0) {
                if (first.hasNext() && second.hasNext()) {
                    ret = first.next().toString()
                            .compareToIgnoreCase(second.next().toString());
                } else {
                    return o1.size() - o2.size();
                }
            }
            return ret;
        });
        words.clear();
        lettersList.forEach(list -> words.add(new Word(list.stream()
                .map(String::valueOf)
                .collect(Collectors.joining()))));
        words.forEach(logger::info);
    }

    @Override
    public void sortByPercentageOfVowelLetters() {
        List<Word> words = new ArrayList<>(textModel.getAllWorlds());
        Collections.sort(words,
                Comparator.comparingDouble(textModel::getPercentOfVowelLetters));
        words.forEach(logger::info);
    }

    @Override
    public void sortVowelWordsBy1stConsonantLetter() {
        Set<Word> words = textModel.getAllWorlds().stream()
                .filter(word -> textModel.checkIfVowel(word))
                .collect(Collectors.toSet());
        List<Word> sortedWords = new ArrayList<>(words);
        words.forEach(logger::info);
        Collections.sort(sortedWords, (o1, o2) -> {
            int ret;
            try {
                ret = textModel.findFirstConsonantLetter(o1)
                        .compareToIgnoreCase(textModel.findFirstConsonantLetter(o2));
            } catch (NullPointerException e) {
                if (textModel.findFirstConsonantLetter(o1) == null)
                    return 1;
                else return -1;
            }
            return ret;
        });
        sortedWords.forEach(logger::info);
    }

    @Override
    public void sortByLetterOccurrenceIncreasing(Character c) {
        List<Word> words = new ArrayList<>(textModel.getAllWorlds());
        Collections.sort(words, (o1, o2) -> {
            int ret = 0;
            if (o1.getLetters().containsKey(c)
                    && o2.getLetters().containsKey(c))
                ret = o1.getLetters().get(c) - o2.getLetters().get(c);
            else if (o1.getLetters().containsKey(c)
                    && !o2.getLetters().containsKey(c))
                ret = 1;
            else if (!o1.getLetters().containsKey(c)
                    && o2.getLetters().containsKey(c))
                ret = -1;
            else return 1;
            Iterator<Character> first = o1.getLetterList().iterator();
            Iterator<Character> second = o2.getLetterList().iterator();
            while (ret == 0) {
                if (first.hasNext() && second.hasNext()) {
                    String let1 = first.next().toString();
                    String let2 = second.next().toString();
                    ret = let1.compareToIgnoreCase(let2);
                } else {
                    return o1.getLength() - o2.getLength();
                }
                ;
            }
            return ret;
        });
        words.forEach(logger::info);
    }

    @Override
    public void printWordOccurrences(Set<Word> wordSet) {
        Map<Sentence, Map<Word, Integer>> sentenceOccurrences =
                new LinkedHashMap<>();
        int occurs;
        for (Sentence s : text.getSentences()) {
            sentenceOccurrences.put(s, new HashMap<>());
            for (Word word : wordSet) {
                Map<Word, Integer> currentSentence = new HashMap<>(s.getWordsStat());
                if (currentSentence.containsKey(word))
                    occurs = currentSentence.get(word);
                else occurs = 0;
                sentenceOccurrences.get(s).put(word, occurs);
            }
        }
        sentenceOccurrences.forEach((sentence, map) -> {
            logger.info(sentence);
            map.entrySet().stream().forEach((k) -> logger.info(k.getKey()
                    + " occurs " + k.getValue()));
        });
        Map<Word, Integer> totalOccurrences = new LinkedHashMap<>();
        for (Word word : wordSet) {
            totalOccurrences.put(word, 0);
            for (Map<Word, Integer> sentenceMap : sentenceOccurrences.values()) {
                occurs = totalOccurrences.get(word).intValue()
                        + sentenceMap.get(word).intValue();
                totalOccurrences.put(word, occurs);
            }
        }
        List<Word> sortedByOccurrences = new ArrayList<>(wordSet);
        Collections.sort(sortedByOccurrences, Collections.reverseOrder(
                Comparator.comparingInt(o -> totalOccurrences.get(o))));
        sortedByOccurrences.forEach(word ->
                logger.info(word.getWord() + ", "
                        + totalOccurrences.get(word)));
    }

    @Override
    public void deleteLongestSubstring(Character c) {
        List<Sentence> sentences = text.getSentences();
        String regex = c.toString() + "(.+)" + c.toString();
        for (Sentence s : sentences) {
            String senString = s.getSentence();
            logger.info(s.getSentence());
            Pattern subs = Pattern.compile(regex);
            Matcher m = subs.matcher(senString);
            if (m.find()) {
                logger.warn(senString.substring(m.start(), m.end()));
                StringBuilder sb = new StringBuilder(senString);
                sb = sb.replace(m.start(), m.end(), "");
                s.setSentence(sb.toString());
            }
        }
    }

    @Override
    public void deleteConsonantsByLength(int length) {
        textModel.getText().getSentences().forEach(logger::info);
        Set<Word> words = textModel.getAllWorlds().stream()
                .filter(word -> !textModel.checkIfVowel(word)
                        && word.getLength() == length)
                .collect(Collectors.toSet());
        words.forEach(word -> textModel.removeWordFromText(word));
        text.getSentences().forEach(logger::info);
    }

    @Override
    public void sortByLetterOccurrenceDecreasing(Character c) {
        List<Word> words = new ArrayList<>(textModel.getAllWorlds());
        Collections.sort(words, (o1, o2) -> {
            int ret = 0;
            if (o1.getLetters().containsKey(c)
                    && o2.getLetters().containsKey(c))
                ret = o2.getLetters().get(c) - o1.getLetters().get(c);
            else if (!o1.getLetters().containsKey(c)
                    && o2.getLetters().containsKey(c))
                ret = 1;
            else return -1;
            Iterator<Character> first = o1.getLetterList().iterator();
            Iterator<Character> second = o2.getLetterList().iterator();
            while (ret == 0) {
                if (first.hasNext() && second.hasNext()) {
                    String let1 = first.next().toString();
                    String let2 = second.next().toString();
                    ret = let1.compareToIgnoreCase(let2);
                } else {
                    return o1.getLength() - o2.getLength();
                }
            }
            return ret;
        });
        words.forEach(logger::info);
    }

    @Override
    public void findLongestPalindromeInEachSentence() {
        List<Sentence> sentences = text.getSentences();
        StringBuilder temp;
        for (Sentence s : sentences) {
            StringBuilder biggest = new StringBuilder("");
            Deque<Character> sentenceChars = new ArrayDeque<>();
            for (char c: s.getSentence().toCharArray()) {
                sentenceChars.add(c);
            }
            while(sentenceChars.size() >2){
                if(checkIfPalindrome(sentenceChars)){
                    temp = getPalindrome(sentenceChars);
                    if (temp.toString().length() > biggest.toString().length()){
                        biggest = temp;
                    }
                }
                sentenceChars.pollFirst();
                Deque<Character> chars = new ArrayDeque<>(sentenceChars);
                while(chars.size()>2){
                    chars.pollLast();
                    temp = getPalindrome(chars);
                    if(checkIfPalindrome(chars)){
                        if (temp.toString().length() > biggest.toString().length()){
                            biggest = temp;
                        }
                    }
                }
            }
            logger.info(s.getSentence());
            logger.info("Longest palindrome -> " + biggest.toString());
        }
    }

    private boolean checkIfPalindrome(Deque<Character> palindromeChars){
        StringBuilder palindrome = getPalindrome(palindromeChars);
        if (palindrome.toString().equals(palindrome.reverse().toString())
            && palindrome.toString().length()>=3){
            return true;
        }
        if (palindromeChars.size()<2){
            logger.info("Palindrome not found");
        }
        return false;
    }

    private StringBuilder getPalindrome(Deque<Character> palindromeChars){
        StringBuilder palindrome = new StringBuilder();
        palindromeChars.forEach(c -> palindrome.append(c));
        return new StringBuilder(palindrome.toString()
                .replace(" ", ""));
    }
}

