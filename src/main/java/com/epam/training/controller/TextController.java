package com.epam.training.controller;

import com.epam.training.model.Word;
import java.util.Set;

public interface TextController {
    void findMaxNumberOfSentencesWithSameWord();                         //1
    void printSentencesInIncreasingOrder();                              //2
    void findUniqueWordInFirstSentence();                                //3
    void printAllWordsFromQuestionsBy(int length);                       //4
    void swapFirstWordStartsByVowelWithLongest();                        //5
    void printWordsInAlphabetOrder();                                    //6
    void sortByPercentageOfVowelLetters();                               //7
    void sortVowelWordsBy1stConsonantLetter();                           //8
    void sortByLetterOccurrenceIncreasing(Character character);          //9
    void printWordOccurrences(Set<Word> wordSet);                        //10
    void deleteLongestSubstring(Character character);                    //11
    void deleteConsonantsByLength(int length);                           //12
    void sortByLetterOccurrenceDecreasing(Character character);          //13
    void findLongestPalindromeInEachSentence();                          //14
}

