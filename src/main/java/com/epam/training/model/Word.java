package com.epam.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Word {
    static final Logger logger = LogManager.getLogger(Word.class.getName());
    private String word;
    private Map<Character, Integer> letters;
    private List<Character> letterList;
    private int length;

    public Word(String word) {
        this.word = word;
        letters = new LinkedHashMap<>();
        letterList = new LinkedList<>();
        fillLetters();
    }

    private void fillLetters(){
        letterList.clear();
        letters.clear();
        for (int i = 0; i < word.toCharArray().length; i++) {
            char letter = word.toLowerCase().toCharArray()[i];
            if (letters.keySet().contains(letter)){
                letters.put(letter, letters.get(letter)+1);
                letterList.add(letter);
            } else {
                letters.put(letter, 1);
                letterList.add(letter);
            }
        }
        length = letterList.size();
    }

    public List<Character> getLetterList(){
        return letterList;
    }

    public String getWord() {
        return word;
    }

    public Map<Character, Integer> getLetters(){
        return letters;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return word.equalsIgnoreCase(word1.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }
}
