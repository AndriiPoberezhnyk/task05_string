package com.epam.training.model;

import java.util.Set;

public interface TextModel {
    long countSentencesWithWord(Word word);
    Sentence swapWords(Sentence sentence);
    Set<Word> getWordsByLength(Sentence sentence, int length);
    boolean checkIfSentencesContain(Word word);
    Set<Word> getAllWorlds();
    Text getText();
    void setText(Text text);
    double getPercentOfVowelLetters(Word word);
    boolean checkIfVowel(Word word);
    String findFirstConsonantLetter(Word word);
    void removeWordFromText(Word word);

}
