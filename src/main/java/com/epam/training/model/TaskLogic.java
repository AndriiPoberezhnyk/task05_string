package com.epam.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskLogic implements TextModel {
    static final Logger logger = LogManager.getLogger(TaskLogic.class.getName());
    private Text text;

    public TaskLogic() {
        text = new Text();
    }

    @Override
    public Sentence swapWords(Sentence sentence) {
        Word longestWord = findLongestWord(sentence);
        Word firstStartsByVowel = findWordStartsWithVowel(sentence);
        logger.info("Swap ->" + longestWord + " by " + firstStartsByVowel);
        if (longestWord != null && firstStartsByVowel != null) {
            swapWordsInList(longestWord, firstStartsByVowel, sentence);
            swapWordsInStringSentence(longestWord, firstStartsByVowel,
                    sentence);
            return sentence;
        }
        return sentence;
    }

    @Override
    public Set<Word> getWordsByLength(Sentence sentence, int length) {
        return sentence.getWords().stream()
                .filter(word -> word.getWord().length() == length)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean checkIfSentencesContain(Word word) {
        List<Sentence> sentences = text.getSentences();
        int occurs = (int) sentences.stream()
                .filter(sentence -> sentence.getWords().contains(word))
                .count();
        if (occurs == sentences.size())
            return true;
        else
            return false;
    }

    @Override
    public long countSentencesWithWord(Word word) {
        List<Sentence> sentences = text.getSentences();
        return sentences.stream()
                .filter(sentence -> sentence.getWords().contains(word))
                .count();
    }

    @Override
    public Set<Word> getAllWorlds() {
        return text.getWordSet();
    }

    @Override
    public double getPercentOfVowelLetters(Word word) {
        String regex = "[aeiouy]";
        long vowelLetters = word.getLetterList().stream()
                        .filter(c -> c.toString().toLowerCase()
                        .matches(regex))
                        .count();
        return vowelLetters*1.0/word.getLength();
    }

    @Override
    public boolean checkIfVowel(Word word) {
            if (word.getWord().toLowerCase().matches("^[aeiouy]\\w*(’\\w+)?"))
                return true;
            else return false;
    }

    @Override
    public String findFirstConsonantLetter(Word word) {
        String wordStr = word.getWord();
        Pattern consLetter = Pattern.compile("[^aeiouyAEIOUY]");
        Matcher m = consLetter.matcher(wordStr);
        if (m.find()){
            return wordStr.substring(m.start(),m.end());
        }else {
            return null;
        }
    }

    @Override
    public void removeWordFromText(Word word) {
        String regex = "\\Wword\\W|^word\\W";
        for (Sentence s: text.getSentences()) {
            StringBuilder sntcBuilder = new StringBuilder(s.getSentence());
            Pattern wPattern =
                    Pattern.compile(regex.replace("word", word.getWord()));
            Matcher m = wPattern.matcher(s.getSentence());
            int removedIndex = 0;
            while(m.find()){
                int start = m.start();
                if (start!=0){
                    start++;
                }
                sntcBuilder.delete(start-removedIndex,
                        m.end()-removedIndex);
                removedIndex += m.end() -m.start();
                logger.info(word.getWord());
                logger.warn(sntcBuilder.toString());
            }
            s.setSentence(sntcBuilder.toString());
        }
    }

    private void swapWordsInStringSentence(Word longest, Word startsByVowel,
                                           Sentence sentence){
        String regex = "\\Wword\\W|^word\\W";
        StringBuilder newSentence = new StringBuilder(sentence.getSentence());
        Pattern longestWordPattern = Pattern
                .compile(regex.replace("word", longest.getWord()));
        Pattern vowelWordPattern = Pattern
                .compile(regex.replace("word", startsByVowel.getWord()));
        Matcher longestW =
                longestWordPattern.matcher(newSentence.toString());
        Matcher vowelW =
                vowelWordPattern.matcher(newSentence.toString());
        if (longestW.find()) {
            if (vowelW.find()) {
                int longStart = longestW.start() == 0 ? 0 :
                        longestW.start() + 1;
                int vowelStart = vowelW.start() == 0 ? 0 :
                        vowelW.start() + 1;
                if (vowelW.start() > longestW.start()) {
                    newSentence.replace(vowelStart, vowelW.end(),
                            longest.getWord() + " ");
                    newSentence.replace(longStart, longestW.end(),
                            startsByVowel.getWord() + " ");
                } else {
                    newSentence.replace(longStart, longestW.end(),
                            startsByVowel.getWord() + " ");
                    newSentence.replace(vowelStart, vowelW.end(),
                            longest.getWord() + " ");
                }
            }
        }
        sentence.setSentence(newSentence.toString());
    }

    private void swapWordsInList(Word longest, Word startsByVowel,
                                 Sentence sentence){
        int indexLongest = sentence.getWords().indexOf(longest);
        int indexVowel = sentence.getWords().indexOf(startsByVowel);
        sentence.getWords().set(indexLongest, startsByVowel);
        sentence.getWords().set(indexVowel, longest);
    }

    private Word findLongestWord(Sentence sentence) {
        return sentence.getWords().stream().reduce((word, word2) -> {
            if (word.getWord().length() > word2.getWord().length())
                return word;
            else
                return word2;
        }).get();
    }

    private Word findWordStartsWithVowel(Sentence sentence) {
        for (Word word : sentence.getWords()) {
            if (checkIfVowel(word)) {
                return word;
            }
        }
        logger.warn("Word which starts with vowel doesn't found");
        return null;
    }

    @Override
    public Text getText() {
        return text;
    }

    @Override
    public void setText(Text text) {
        this.text = text;
    }

}
