package com.epam.training.model;

import com.epam.training.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
    static final Logger logger = LogManager.getLogger(Sentence.class.getName());
    private String sentence;
    private List<Word> words;
    private Map<Word, Integer> wordsStat;

    public Sentence(String sentence) {
        this.sentence = sentence;
        words = new ArrayList<>();
        wordsStat = new LinkedHashMap<>();
        setWords();
    }

    public void setSentence(String sentence){
        this.sentence = sentence;
        setWords();
    }

    public String getSentence() {
        return sentence;
    }

    @Override
    public String toString() {
        return "Sentence='" + sentence;
    }

    private void setWords(){
        words.clear();
        wordsStat.clear();
        Pattern pattern = Pattern.compile("[A-Za-z]+([’']\\w+)*");
        Matcher matcher = pattern.matcher(sentence);
        while(matcher.find()){
            Word word = new Word(sentence
                    .substring(matcher.start(), matcher.end()));
            if (words.contains(word)){
                int occurrences = wordsStat.get(word).intValue();
                wordsStat.put(word, occurrences+1);
            }else{
                words.add(word);
                wordsStat.put(word, 1);
            }
        }
    }

    public Map<Word,Integer> getWordsStat(){
        return wordsStat;
    }

    public List<Word> getWords(){
        return words;
    }

    public int getSentenceLength(){
        return words.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence1 = (Sentence) o;
        return sentence.equalsIgnoreCase(sentence1.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence);
    }
}
