package com.epam.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    private static final Logger logger =
            LogManager.getLogger(Text.class.getName());
    private String textFromFile;
    private List<Sentence> sentences;
    private Set<Word> words;

    public Text(){
        try {
            textFromFile = getTextFromFile();
        } catch (IOException e) {
            logger.error("Reading file error");
        }
        sentences = new ArrayList<>();
        words = new HashSet<>();
        setSentencesFromFile();
        fillWordSet();
    }

    private String getTextFromFile() throws IOException {
        StringBuilder sb = new StringBuilder();
        try (FileReader reader = new FileReader("text.txt");
             BufferedReader br = new BufferedReader(reader)) {
            br.lines().forEach(line -> {
                if (!line.endsWith(" ")) sb.append(line + " ");
                else sb.append(line);
            });
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
        return sb.toString();
    }

    private void setSentencesFromFile(){
        Pattern pattern = Pattern.compile("\\w+[^.?!:]+(\\.\\w+[^.?!:]+)*[" +
                ".?!:]");
        Matcher matcher = pattern.matcher(textFromFile);
        while(matcher.find()){
            sentences.add(new Sentence(textFromFile
                    .substring(matcher.start(), matcher.end())));
        }
    }

    private void fillWordSet(){
        sentences.stream().forEach(sentence -> {
            sentence.getWords().stream().forEach(word -> words.add(word));
        });
    }

    public Set<Word> getWordSet(){
        return words;
    }

    public void setSentences(List<Sentence> sentences){
        this.sentences = sentences;
    }

    public List<Sentence> getSentences(){
        return sentences;
    }
}
